class Group < ApplicationRecord
  has_many :events
  has_attached_file :logo, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  validates :title, presence: true
  validates_attachment :logo,
      content_type: { content_type: ["image/jpeg", "image/gif", "image/png"] }
end
