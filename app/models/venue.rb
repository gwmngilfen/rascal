class Venue < ApplicationRecord
  has_many :events

  geocoded_by :address

  validates :title, presence: true
  validates :address, presence: true

  # Don't waste API calls if the address hasn't changed
  after_validation :geocode, if: ->(obj){ obj.address.present? and obj.address_changed? }
end
