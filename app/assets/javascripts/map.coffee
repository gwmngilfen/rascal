jQuery ->
  markersArray = []
  lat_field = $('#venue_latitude')
  lng_field = $('#venue_longitude')
  zm_field  = $('#venue_zoom')
  address   = $('#venue_address')

  window.initMap = ->
    if $('#map').size() > 0
      map = new google.maps.Map document.getElementById('map'), {
        center: {lat: 56.118, lng: -3.9496}
        zoom: Number(zm_field.val())
      }
      placeMarkerAndPanTo {lat: Number(lat_field.val()), lng: Number(lng_field.val())} , map

      map.addListener 'click', (e) ->
        placeMarkerAndPanTo e.latLng, map
        updateFields e.latLng, map.getZoom()

      $('#find-on-map').click (e) ->
        e.preventDefault()
        placeMarkerAndPanTo {
          lat: Number(lat_field.val())
          lng: Number(lng_field.val())
        }, map
        map.setZoom(Number(if (zm_field.val() == "") then "12" else zm_field.val()))

  placeMarkerAndPanTo = (latLng, map) ->
    markersArray.pop().setMap(null) while(markersArray.length)
    marker = new google.maps.Marker
      position: latLng
      map: map

    map.panTo latLng
    markersArray.push marker

  getGeocodedAddress = (latLng) ->
    $.getJSON('/venues/find_address_by_latlong', 
    {
      lat: latLng.lat(),
      long: latLng.lng()
    })
    .done (data) ->
      address.val data.address
      return
    .fail (jq_xhr, status, error) ->
      # nothing yet, add handling
      return

  updateFields = (latLng, zoom) ->
    lat_field.val latLng.lat()
    lng_field.val latLng.lng()
    getGeocodedAddress(latLng)
    zm_field.val zoom
