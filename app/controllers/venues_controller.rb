class VenuesController < ApplicationController
  def index
    @venues = Venue.all
    respond_to do |format|
      format.html
      format.json { render json: @venues}
    end
  end

  def show
    @venue = Venue.find(params[:id])
    respond_to do |format|
      format.html
      format.json { render json: @venue}
    end
  end

  def new
    @venue = Venue.new
  end

  def edit
    @venue = Venue.find(params[:id])
  end

  def create
    @venue = Venue.new(venue_params)

    if @venue.save
      redirect_to @venue
    else
      render 'new'
    end
  end

  def update
    @venue = Venue.find(params[:id])

    if @venue.update(venue_params)
      redirect_to @venue
    else
      render 'edit'
    end
  end

  def destroy
    @venue = Venue.find(params[:id])
    @venue.destroy

    redirect_to venues_path
  end

  def find_address_by_latlong
    r = Geocoder.search([params[:lat],params[:long]])
    a = r.detect{|x| x.data['types'] == ['postal_code']}.data['formatted_address']
    render :json => "{ \"address\": \"#{a}\" }"
  end

  private
  def venue_params
    params.require(:venue).permit(:title, :description, :latitude, :longitude, :url, :address, :zoom)
  end
end
