class EventsController < ApplicationController
  def index
    @events = []
    if params[:city] && params[:dist]
      venues = Venue.near(params[:city],params[:dist])
      events = venues.map { |v| v.events }
      @events = events.flatten
    else
      @events = Event.all
    end
    respond_to do |format|
      format.html
      format.json { render json: @events}
    end
  end

  def show
    @event = Event.find(params[:id])
    respond_to do |format|
      format.html
      format.json { render json: @event}
    end
  end

  def new
    @event = Event.new
  end

  def edit
    @event = Event.find(params[:id])
  end

  def create
    @event = Event.new(event_params)

    if @event.save
      redirect_to @event
    else
      render 'new'
    end
  end

  def update
    @event = Event.find(params[:id])

    if @event.update(event_params)
      redirect_to @event
    else
      render 'edit'
    end
  end

  def destroy
    @event = Event.find(params[:id])
    @event.destroy

    redirect_to events_path
  end

  private
  def event_params
    params.require(:event).permit(:title, :description, :group_id, :venue_id, :url)
  end
end
