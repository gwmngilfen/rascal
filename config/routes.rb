Rails.application.routes.draw do
  get  'welcome/index'
  root 'welcome#index'

  resources :events
  resources :groups
  resources :venues do
    collection do
      get 'find_address_by_latlong' 
    end
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
