# README

Rascal is an application which allows users to share events they are aware of to
a shared calendar that everyone can see. This allows for everyone to contribute
to the knowledge of events in an area.

## Rascal?

The name is simply *RA*ils *S*hared *CAL*endar. Better names are welcome :)

## Install

Bundle install is the only supported option so far. More docs to come!

## Setup

`/bin/rails server` and then head to http://localhost:3000

## Usage

The app is in early stages just now, docs will follow ;)

## License

The Rascal repository is licensed under the GNU GPL v3

Copyright (c) 2016 to Greg Sutcliffe

This program and entire repository is free software: you can redistribute it
and/or modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see [GNU licenses](http://www.gnu.org/licenses/).
