class AddZoomToVenue < ActiveRecord::Migration[5.0]
  def change
    add_column :venues, :zoom, :integer
  end
end
