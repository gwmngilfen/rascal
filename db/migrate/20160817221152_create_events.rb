class CreateEvents < ActiveRecord::Migration[5.0]
  def change
    create_table :events do |t|
      t.string :title
      t.text :description
      t.string :url
      t.references :group, foreign_key: true
      t.references :venue, foreign_key: true

      t.timestamps
    end
  end
end
