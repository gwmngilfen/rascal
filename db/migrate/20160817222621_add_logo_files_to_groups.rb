class AddLogoFilesToGroups < ActiveRecord::Migration[5.0]
  def up
    add_attachment :groups, :logo
  end

  def down
    remove_attachment :groups, :logo
  end
end
