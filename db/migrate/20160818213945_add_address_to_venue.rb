class AddAddressToVenue < ActiveRecord::Migration[5.0]
  def change
    add_column :venues, :address, :text
  end
end
